package org.apache.jmeter.protocol.mqtt.client;


import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;
import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.client.Listener;

public class ListenerforSubscribe implements Listener {

	public static AtomicLong count= new AtomicLong(0); 
	public int cnt;
	public ArrayList<String> message = new ArrayList<String>();
	public ArrayList<String> topic = new ArrayList<String>();
	
	@Override
	public void onConnected() {
		System.out.println("Subscriber is listening");

	}

	@Override
	public void onDisconnected() {
		System.out.println("Subscriber disabled listening");

	}

	@Override
	public void onPublish(UTF8Buffer topic, Buffer body, Runnable ack) {
		String message = new String(body.getData());
		String topicString = new String(topic.getData());
		
		count.getAndIncrement();		
		cnt++;
		// Assume only one topic will be received per call.
		// TODO: Fix this properly
		// The first 5 char will hold the Packet ID(4) & QOS (1)
		this.message.add(message.substring(message.indexOf('{'), message.length()));
		this.topic.add(topicString.substring(5, topicString.indexOf('{')));
		//System.out.println("Received: " + message.substring(message.indexOf('{'), message.length()));
		//System.out.println("on TOPIC: " + topicString.substring(5, topicString.indexOf('{')));
		ack.run();

	}

	@Override
	public void onFailure(Throwable value) {
		System.out.println("Subscriber couldn't set up listener");
		System.out.println(value);
	}

}
